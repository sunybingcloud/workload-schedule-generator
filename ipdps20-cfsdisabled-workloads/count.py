#!/usr/bin/python3

import argparse
import json
import collections

def get_args():
    parser = argparse.ArgumentParser(description="Count the total number of instances in the workload")
    required = parser.add_argument_group('required arguments')
    required.add_argument('-w', '--workload', type=str, help='path to workload', required=True)
    required.add_argument('-b', '--benchmark', type=str, help='benchmark for which the number of instances needs to be counted')
    required.add_argument('-ws', '--workload-section', type=str, help='workload section for which the number of instances needs to be counted')
    args = parser.parse_args()
    return args

def count_total_instances(args, job):
    total_count = 0
    for task_definition in job:
        if args.benchmark:
            if args.benchmark == task_definition['name']:
                if 'inst' in task_definition:
                    total_count += int(task_definition['inst'])
        else:
            if 'inst' in task_definition:
                total_count += int(task_definition['inst'])
    return total_count

def valid_job(job):
    return not str(job).isdigit()

can_count = True
def main():
    global can_count
    args = get_args()
    inst_count_workload = 0
    with open(args.workload, 'r') as f:
        workload_schedule_content = f.readlines()
        for line in workload_schedule_content:
            inst_count_job = 0
            try:
                job = json.loads(line)
                if valid_job(job):
                    inst_count_job = count_total_instances(args, job)
            except:
                # If an exception occurred, then just skipping line as it's not a json.
                if args.workload_section != line.strip():
                    if "End " + args.workload_section != line.strip():
                        can_count = False
                else:
                    can_count = True

            if can_count:
                inst_count_workload += inst_count_job

    print(inst_count_workload)

if __name__ == "__main__":
    main()
