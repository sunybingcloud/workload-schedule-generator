FROM python:2.7

ADD *.py /

RUN pip install requests_toolbelt

ENTRYPOINT [ "python", "./main.py" ]

