# Power specifications provided for all runs so that utilization and allocation metrics be logged.
powerSpecs="./powerSpecs.json"
# Arguments to run electron with a given scheduling policy.
# Workloads to be submitted to electron.
workloadNoincExtimeMedmedmedpuSchedule="noincExtime-tolerance-workloads/workload_medmedmedpu_noincExtimeTolerance_schedule"
workloadNoincExtimeMedmedmaxpeakpuSchedule="noincExtime-tolerance-workloads/workload_medmedmaxpeakpu_noincExtimeTolerance_schedule"

workload10percincExtimeMedmedmedpuSchedule="10percExtime-tolerance-workloads/workload_medmedmedpu_10percExtimeTolerance_schedule"
workload10percincExtimeMedmedmaxpeakpuSchedule="10percExtime-tolerance-workloads/workload_medmedmaxpeakpu_10percExtimeTolerance_schedule"

workload20percincExtimeMedmedmedpuSchedule="20percExtime-tolerance-workloads/workload_medmedmedpu_20percExtimeTolerance_schedule"
workload20percincExtimeMedmedmaxpeakpuSchedule="20percExtime-tolerance-workloads/workload_medmedmaxpeakpu_20percExtimeTolerance_schedule"

# Common part of the logprefix for all runs.
logprefixcommon=$1

# FIRST-FIT
ffpolicyname="first-fit"
# The prefix that is specific to First-Fit. This is going to be used to create a logprefix.
fflogprefix="Electron-FF"

# Arguments to be provided when running electron with first-fit.
declare -A ff=( [sp]=$ffpolicyname [logprefix]=$fflogprefix )

# BIN-PACKING
bppolicyname="bin-packing"
# The prefix that is specific to Bin-Packing. This is going to be used to create a logprefix.
bplogprefix="Electron-BP"

# Arguments to be provided when running electron with bin-packing.
declare -A bp=( [sp]=$bppolicyname [logprefix]=$bplogprefix )

# MAX-MIN
mmpolicyname="max-min"
# The prefix that is specific to Max-Min. This is going to be used to create a logprefix.
mmlogprefix="Electron-MM"

# Arguments to be provided when running electron with max-min.
declare -A mm=( [sp]=$mmpolicyname [logprefix]=$mmlogprefix )

# MAX-GREEDYMINS
mgmpolicyname="max-greedymins"
# The prefix that is specific to Max-GreedyMins. This is going to be used to create a logprefix.
mgmlogprefix="Electron-MGM"

# Arguments to be provided when running electron with max-min.
declare -A mgm=( [sp]=$mgmpolicyname [logprefix]=$mgmlogprefix )

# Watts as a Resource
waarpolicyname="waar-scheduler"
# The prefix to be provide to WaaR. This is going to be used to create a logprefix.
waarlogprefix="Electron-WaaR"

# Arguments to be provided when running electron with waar-scheduler.
declare -A waar=( [sp]=$waarpolicyname [logprefix]=$waarlogprefix )

# Running electron with first-fit
common="./electron -w $workload -powerSpecs $powerSpecs"

# streamer.
function start_streaming {
	# waiting for electron to register with mesos and start receiving mesos resource offers.
	sleep 10
	python main.py $1 --addr xavier:4545 --existing --log INFO
}

# NOINC-EXECUTION_TIME-TOLERANCE RUNS.
# ===================================================================================================================================
# 1. median of medians of median power usage profiling.
# -------------------------------------------------------
ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmedpu-noincExtimeTol"" > /dev/null 2>&1 &
# launching the streamer.
start_streaming $workloadNoincExtimeMedmedmedpuSchedule
echo "done - waar-scheduler noincextime tolerance medmedmedpu."

## 2. median of medians of max peak power usage profiling.
## -------------------------------------------------------
#ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmaxpeakpu-noincExtimeTol"" > /dev/null 2>&1 &
## launching the streamer.
#start_streaming $workloadNoincExtimeMedmedmaxpeakpuSchedule
## waiting for tasks to complete execution and for electron to terminate.
#wait_till_tasks_complete
#echo "done - noincextime tolerance medmedmaxpeakpu."
#
## 10PERCINC-EXECUTION_TIME-TOLERANCE RUNS.
## ===================================================================================================================================
## 1. median of medians of median power usage profiling.
## -------------------------------------------------------
#ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmedpu-10percincExtimeTol"" > /dev/null 2>&1 &
## launching the streamer.
#start_streaming $workload10percincExtimeMedmedmedpuSchedule
## waiting for tasks to complete execution and for electron to terminate.
#wait_till_tasks_complete
#echo "done - 10percextime tolerance medmedmedpu."
#
## 2. median of medians of max peak power usage profiling.
## -------------------------------------------------------
#ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmaxpeakpu-10percincExtimeTol"" > /dev/null 2>&1 &
## launching the streamer.
#start_streaming $workload10percincExtimeMedmedmaxpeakpuSchedule
## waiting for tasks to complete execution and for electron to terminate.
#wait_till_tasks_complete
#echo "done - 10percextime tolerance medmedmaxpeakpu."
#
## 20PERCINC-EXECUTION_TIME-TOLERANCE RUNS.
## ===================================================================================================================================
## 1. median of medians of median power usage profiling.
## -------------------------------------------------------
#ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmedpu-20percincExtimeTol"" > /dev/null 2>&1 &
## launching the streamer.
#start_streaming $workload20percExtimeMedmedmedpuSchedule
## waiting for tasks to complete execution and for electron to terminate.
#wait_till_tasks_complete
#echo "done - 20percextime tolerance medmedmedpu."
#
## 2. median of medians of max peak power usage profiling.
## -------------------------------------------------------
#ssh xavier "cd runelektron-waar && ./electron -httpServer -port 4545 -sp ${waar[sp]} -powerSpecs $powerSpecs -alignScore cpuPowerSlack -p "${waar[logprefix]}-medmedmaxpeakpu-20percincExtimeTol"" > /dev/null 2>&1 &
## launching the streamer.
#start_streaming $workload20percincExtimeMedmedmaxpeakpuSchedule
## waiting for tasks to complete execution and for electron to terminate.
#wait_till_tasks_complete
#echo "done - 20percextime tolerance medmedmaxpeakpu."
