import os
import sys
import json
import logging

DEFAULT_OUTPUT_FILE = "schedule.txt"


def parse_file(filepath):

    verify_file_exists(filepath)

    with open(filepath, "r") as json_file:
        data = json.load(json_file)

    return data


def verify_file_exists(filepath):
    if not os.path.isfile(filepath):
        logging.critical("Error: the input file %s doesn't exist.\n", str(filepath))
        sys.exit(1)


def write_to_output(data, output_file):
    if not output_file:
        output_file = DEFAULT_OUTPUT_FILE
    handle = open(output_file, 'w')
    handle.write(data)
    handle.close()
    logging.info("Schedule written to %s" % output_file)


def dump(obj):
    return json.dumps(obj)


def parse_schedule(filepath):

    verify_file_exists(filepath)

    output = []
    with open(filepath, "r") as schedule_file:
        for line in schedule_file:
            line = line.strip()

            if line.isdigit():
                # The line contains a sleep time (Integer)
                sleep_time = int(line)
                output.append(sleep_time)

            else:
                try:
                    # The line contains a job (JSON format)
                    job = json.loads(line)
                    output.append(job)

                except ValueError:
                    # THe line contains a section tag (string)
                    output.append(line)

    return output


def parse_custom_workload(custom_workload_filepath):

    custom_workload_data = parse_file(custom_workload_filepath)

    # The following section can be changed in the future. Currently we're supporting only custom tolerance values.
    tolerance_dict = {}

    assert(isinstance(custom_workload_data, dict))

    '''
    Format of custom workload file:
    {
        "Section Name":
        [
            {"name": NAME1, "tolerance": TOLERANCE1},
            {"name": NAME2, "tolerance": TOLERANCE2},
            ...
    
        ],
        "Section Name 2":
        [
            {...},
            {...}
        ]
    }
    '''
    for section_name, section_custom_workload_data in custom_workload_data.items():

        if section_name not in tolerance_dict:
            tolerance_dict[section_name] = {}

        for task_info in section_custom_workload_data:
            task_name = task_info["name"]
            tolerance = task_info["tolerance"]

            tolerance_dict[section_name][task_name] = tolerance

    return tolerance_dict

