import sys
import requests
from requests_toolbelt import MultipartEncoder
import time
import utils
import logging

JOB_SUBMIT_URL = "http://%s/submit"
SHUTDOWN_URL = "http://%s/shutdown"


def post_streaming_schedule(host, schedule):
    if not host:
        logging.critical("Error. Please provide Electron server address in IP:Port format as -ip parameter\n")
        sys.exit(1)

    url = JOB_SUBMIT_URL % host

    count = 1
    current_section = ""
    for idx, item in enumerate(schedule):
        if isinstance(item, int):
            # item is sleep time
            if idx == len(schedule)-1:
                break
            logging.info("Sleeping for %d seconds..." % item)
            time.sleep(item)
        elif isinstance(item, str):
            # item is section tag
            logging.info("Section: %s..." % item)
            current_section = item
        else:
            # item is a job
            logging.info("Submitting job %d to URL %s with tag %s..." % (count, url, current_section))
            data = {"job": utils.dump(item), "section": current_section}
            send_post_request(url, data)

            count += 1

    url = SHUTDOWN_URL % host
    # TODO: Workaround. Remove sleep after Electron issue # 40 is fixed
    time.sleep(5)
    send_post_request(url, None)


def send_post_request(url, payload):
    try:
        if payload is not None:
            d = MultipartEncoder(fields=payload)
            r = requests.post(url=url, data=d, headers={'Content-Type': d.content_type})
        else:
            r = requests.post(url=url)
    except requests.exceptions.ConnectionError:
        logging.critical("Error sending request to the URL %s. Aborting...\n" % url)
        sys.exit(1)

    return r
