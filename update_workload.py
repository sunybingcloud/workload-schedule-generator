#!/usr/bin/python3

import argparse
import json
import collections

def get_args():
    parser = argparse.ArgumentParser(description="Update class_to_watts for tasks in the workload.")
    required = parser.add_argument_group('required arguments')
    required.add_argument('-w', '--workload', type=str, help='path to workload that contains the task definitions that need to be updated', required=True)
    required.add_argument('-u', '--update-with', type=str, help='path to json file containing the key-value pairs to replace the existing ones', required=True)
    required.add_argument('-o', '--output', type=str, help='path to output file to which the updated workload is written. ', required=True)

    args = parser.parse_args()
    return args

def get_update_workload(args, workload):
    updated_task_defs = collections.OrderedDict()
    # storing the task definitions in the workload in an ordereddict for quick updates.
    # using an ordereddict to preserve order of tasks.
    # The key for each task definition is going to be the task-name.
    for task_definition in workload:
        updated_task_defs[task_definition['name']] = task_definition

    with open(args.update_with) as file_with_new_keyvalue_pairs:
        try:
            new_keyvalue_pairs = json.load(file_with_new_keyvalue_pairs)
            # for every task definition, updating the specified key-value pairs.
            for new_keyvalue_for_task in new_keyvalue_pairs:
                for task_name, keyvalue in new_keyvalue_for_task.items():
                    if task_name in updated_task_defs:
                        for key, value in keyvalue.items():
                            # updating value only if key is present in the current task definition.
                            if key in updated_task_defs[task_name]:
                                updated_task_defs[task_name][key] = value
                    else:
                        pass # nothing to update for this task in this batch.
        except:
            print('cannot load {}'.format(args.update_with))

    updated_workload = list()
    for updated_task_def in updated_task_defs.values():
        updated_workload.append(updated_task_def)
    return updated_workload

def valid_workload(workload):
    return not str(workload).isdigit()

def main():
    args = get_args()
    output_file = args.output
    with open(output_file, 'w') as output_file_writer:
        with open(args.workload, 'r') as f:
            workload_schedule_content = f.readlines()
            for line in workload_schedule_content:
                try:
                    workload = json.loads(line)
                    if valid_workload(workload):
                        updated_workload = get_update_workload(args, workload)
                        json.dump(updated_workload, output_file_writer)
                        output_file_writer.write('\n')
                    else:
                        output_file_writer.write(line)
                except:
                    # If an exception occurred, then just writing the line into the output file.
                    output_file_writer.write(line)

if __name__ == "__main__":
    main()
